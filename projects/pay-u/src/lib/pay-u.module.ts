import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpinModule } from 'ng-zorro-antd/spin';

import { PayUComponent } from './pay-u.component';

@NgModule({
  declarations: [
    PayUComponent
  ],
  imports: [
    BrowserModule,
    NzButtonModule,
    NzSpinModule
  ],
  exports: [
    PayUComponent
  ]
})
export class PayUModule { }
