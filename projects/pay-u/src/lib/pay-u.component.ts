import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lib-payU',
  templateUrl: './pay-u.component.html',
  styles: [
  ]
})
export class PayUComponent implements OnInit {

  @Input() config: IPayUConfig | undefined = undefined;
  @Input() debug: boolean = false;
  sending: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  send() {
    console.log('send()');
    this.sending = true;
  }

}

export interface IPayUConfig {
  buyerFullName: string;
  merchantId: string;
  accountId: string;
  description: string;
  referenceCode: string;
  amount: string;
  tax: string;
  taxReturnBase: string;
  currency: string;
  signature: string;
  buyerEmail: string;
  responseUrl: string;
  confirmationUrl: string;
  test?: string;
  extra1?: string;
  textoBoton: string;
}
