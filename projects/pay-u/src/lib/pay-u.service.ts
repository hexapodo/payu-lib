import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5';

@Injectable({
  providedIn: 'root'
})
export class PayUService {

  apiKey: string = '';

  constructor() { }

  setApiKey(apiKey: string): void {
    this.apiKey = apiKey;
  }

  calculateReference(seed: string = 'ticket', len: number = 15): string {
    const md5 = new Md5();
    return seed + '-' + md5.appendStr(Math.random().toString()).end().toString().substr(0, len);
  }

  calculateSignature(merchantId: string, reference: string, amount: string, currency: string): string {
    if (this.apiKey === '') {
      throw new Error('you have to set the apiKey as first step.');
    }
    const md5 = new Md5();
    const text = `${this.apiKey}~${merchantId}~${reference}~${amount}~${currency}`;
    return md5.appendStr(text).end().toString()
  }
}
