import { Component, OnInit } from '@angular/core';
import { IPayUConfig } from 'projects/pay-u/src/lib/pay-u.component';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
}
