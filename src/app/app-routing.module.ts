import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';
import { ConfirmacionComponent } from './confirmacion/confirmacion.component';
import { HomeComponent } from './home/home.component';
import { RespuestaComponent } from './respuesta/respuesta.component';


const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent},
    { path: 'confirmacion', component: ConfirmacionComponent},
    { path: 'respuesta', component: RespuestaComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
