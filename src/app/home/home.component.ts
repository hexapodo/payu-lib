import { Component, OnInit } from '@angular/core';
import { IPayUConfig } from 'projects/pay-u/src/lib/pay-u.component';
import { PayUService } from 'projects/pay-u/src/lib/pay-u.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  payUConfig: IPayUConfig = {
    buyerFullName: '',
    merchantId: '',
    accountId: '',
    description: "",
    referenceCode: '',
    amount: '',
    tax: '',
    taxReturnBase: '',
    currency: '',
    signature: '',
    buyerEmail: '',
    responseUrl: '',
    confirmationUrl: '',
    textoBoton: 'Enviar'
  }

  value: string = '';

  constructor(
    private payUService: PayUService
  ) {}

  ngOnInit(): void {
    this.payUService.setApiKey(environment.apiKey);
    const merchantId = '508029';
    const referenceCode = this.payUService.calculateReference();
    const amount = '750000';
    const currency = 'COP';
    this.payUConfig = {
      ...this.payUConfig,
      buyerFullName: 'APPROVED',
      merchantId,
      accountId: '512321',
      description: 'Ruta Silvia Cauca',
      referenceCode,
      amount,
      tax: '0',
      taxReturnBase: '0',
      currency: 'COP',
      signature: this.payUService.calculateSignature(merchantId, referenceCode, amount, currency),
      buyerEmail: 'jplbazante@gmail.com',
      responseUrl: 'http://localhost:4200',
      confirmationUrl: 'https://www.kishron.com.co',
      test: '1',
    };
  }

}
